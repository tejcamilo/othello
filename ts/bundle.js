define("player", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Player = void 0;
    var Player = /** @class */ (function () {
        function Player(color, score, moves) {
            this.color = color;
            this.score = score;
            this.moves = moves;
        }
        return Player;
    }());
    exports.Player = Player;
    var greeter = new Player("black", 0, 32);
});
define("main", ["require", "exports", "player"], function (require, exports, player_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var player1 = new player_1.Player("black", 0, 32);
});
//# sourceMappingURL=bundle.js.map