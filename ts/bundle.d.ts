declare module "player" {
    export class Player {
        color: string;
        score: number;
        moves: number;
        constructor(color: string, score: number, moves: number);
    }
}
declare module "main" { }
