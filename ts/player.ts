export class Player {
    color: string;
    score: number;
    moves: number;
   
    constructor(color: string, score: number, moves: number) {
      this.color = color;
      this.score = score;
      this.moves = moves;
    }
   

  }
   
  let greeter = new Player("black", 0, 32);